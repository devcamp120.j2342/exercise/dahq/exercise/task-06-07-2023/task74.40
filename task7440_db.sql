-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 08, 2023 at 06:47 PM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `task7440_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `drinks`
--

CREATE TABLE `drinks` (
  `id` int(10) NOT NULL,
  `drink_code` varchar(40) NOT NULL,
  `total_money` int(20) NOT NULL,
  `create_date` int(20) NOT NULL,
  `update_date` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `drinks`
--

INSERT INTO `drinks` (`id`, `drink_code`, `total_money`, `create_date`, `update_date`) VALUES
(1, 'C2', 2000000, 1688834377, 1688834377),
(2, 'OLONG', 3000000, 1688834428, 1688834428),
(3, 'TRAXANH', 2500000, 1688834428, 1688834428),
(4, 'STING', 4000000, 1688834428, 1688834428),
(5, 'TRASUA', 2900000, 1688834428, 1688834428);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) NOT NULL,
  `order_code` varchar(40) NOT NULL,
  `pizza_size` varchar(40) NOT NULL,
  `pizza_type` varchar(40) NOT NULL,
  `voucher_id` int(10) NOT NULL,
  `total_money` int(20) NOT NULL,
  `discount` int(10) NOT NULL,
  `fullname` varchar(40) NOT NULL,
  `email` varchar(40) NOT NULL,
  `address` varchar(40) NOT NULL,
  `drink_id` int(10) NOT NULL,
  `status_id` int(10) NOT NULL,
  `create_date` int(20) NOT NULL,
  `update_date` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `order_code`, `pizza_size`, `pizza_type`, `voucher_id`, `total_money`, `discount`, `fullname`, `email`, `address`, `drink_id`, `status_id`, `create_date`, `update_date`) VALUES
(1, 'YTGJKL', 'L', 'Haiwwai', 2, 2000000, 10, 'NguyenVanA', 'a@gamil.com', 'HG', 2, 2, 1688834751, 1688834751),
(2, 'YTGJKwssL', 'M', 'Bacon', 3, 2400000, 30, 'NguyenVanB', 'b@gamil.com', 'HG', 2, 2, 1688834841, 1688834841),
(3, 'YTOPOYYKL', 'L', 'Haiwwai', 2, 2000000, 10, 'NguyenVanC', 'c@gamil.com', 'HG', 2, 2, 1688834841, 1688834841),
(4, 'TYYKL', 'S', 'Seafood', 5, 6000000, 10, 'NguyenVanD', 's@gamil.com', 'HG', 2, 2, 1688834841, 1688834841),
(5, 'YULKI', 'S', 'Haiwwai', 2, 2000000, 10, 'NguyenVanE', 'e@gamil.com', 'HG', 2, 2, 1688834841, 1688834841);

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `id` int(10) NOT NULL,
  `status_code` varchar(40) NOT NULL,
  `status_name` varchar(40) NOT NULL,
  `create_date` int(20) NOT NULL,
  `update_date` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id`, `status_code`, `status_name`, `create_date`, `update_date`) VALUES
(1, 'ordering', 'open', 1688834478, 1688834478),
(2, 'hello', 'open', 1688834551, 1688834551),
(3, 'Hi', 'open', 1688834551, 1688834551),
(4, 'dilivery', 'cancle', 1688834551, 1688834551),
(5, 'lk', 'open', 1688834551, 1688834551);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) NOT NULL,
  `user_name` varchar(40) NOT NULL,
  `first_name` varchar(40) NOT NULL,
  `last_name` varchar(40) NOT NULL,
  `email` varchar(40) NOT NULL,
  `password` varchar(40) NOT NULL,
  `create_date` int(20) NOT NULL,
  `update_date` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_name`, `first_name`, `last_name`, `email`, `password`, `create_date`, `update_date`) VALUES
(1, 'dahuynh', 'Da', 'Huynh Quang', 'da@gmail.com', '12345', 1688834288, 1688834288),
(2, 'Ahuynh', 'A', 'Huynh', 'a@gmail.com', '12345', 1688834345, 1688834345),
(3, 'Bhuynh', 'B', 'Huynh Quang', 'b@gmail.com', '12345', 1688834346, 1688834346),
(4, 'Chuynh', 'C', 'Huynh Quang', 'c@gmail.com', '12345', 1688834346, 1688834346),
(5, 'Dhuynh', 'D', 'Huynh Quang', 'd@gmail.com', '12345', 1688834346, 1688834346);

-- --------------------------------------------------------

--
-- Table structure for table `vouchers`
--

CREATE TABLE `vouchers` (
  `id` int(10) NOT NULL,
  `voucher_code` varchar(40) NOT NULL,
  `discount` int(20) NOT NULL,
  `is_userd_yn` varchar(40) NOT NULL,
  `create_date` int(10) NOT NULL,
  `update_date` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vouchers`
--

INSERT INTO `vouchers` (`id`, `voucher_code`, `discount`, `is_userd_yn`, `create_date`, `update_date`) VALUES
(1, '12332', 10, 'null', 1688834578, 1688834578),
(2, '1234', 20, 'null', 1688834619, 1688834619),
(3, '1235', 20, 'null', 1688834619, 1688834619),
(4, '12336', 30, 'null', 1688834619, 1688834619),
(5, '12338', 40, 'null', 1688834619, 1688834619);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `drinks`
--
ALTER TABLE `drinks`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `drink_index` (`drink_code`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `order_index` (`order_code`),
  ADD KEY `voucher_id` (`voucher_id`),
  ADD KEY `status_id` (`status_id`),
  ADD KEY `drink_id` (`drink_id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `status_index` (`status_code`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_index` (`user_name`),
  ADD UNIQUE KEY `email_index` (`email`);

--
-- Indexes for table `vouchers`
--
ALTER TABLE `vouchers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `voucher_index` (`voucher_code`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `drinks`
--
ALTER TABLE `drinks`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `vouchers`
--
ALTER TABLE `vouchers`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`voucher_id`) REFERENCES `vouchers` (`id`),
  ADD CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`),
  ADD CONSTRAINT `orders_ibfk_3` FOREIGN KEY (`drink_id`) REFERENCES `drinks` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
